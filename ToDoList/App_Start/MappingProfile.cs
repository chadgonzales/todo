﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ToDoList.Models;

namespace ToDoList.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ToDo, ToDoDTO>();
            CreateMap<ToDoDTO, ToDo>();

        }
    }
}